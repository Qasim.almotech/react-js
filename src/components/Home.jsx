import React, { Component } from "react";
import Table from 'react-bootstrap/Table';
import { Container, Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import axios from 'axios';

class Home extends Component{
    state = {
        posts:[],
    }
    
    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then((response) => {
            this.setState({posts:response.data});
            console.log(response);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })

    }
    
    render(){
        // const posts = this.state.posts;
        const allPosts = this.state.posts.map((post, idx) => {
            return(
                <tr>
                    <td>{ post.id }</td>
                    <td>{ post.title }</td>
                    <td>{ post.userId }</td>
                    <td><Button value={ post.id } variant="outline-info">Trash</Button></td>
                </tr>
            )

        });
        return (
            <Container>
                    <Row>
                        <Col>
                        
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>UserID</th>
                                    <th>options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { allPosts }
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            
        )
    }
} 

export default Home;