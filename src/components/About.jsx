import React from "react";
import Button from 'react-bootstrap/Button';
import { Container, Row, Col } from 'react-bootstrap';

function About(name){
    return (
        <div className="content">
                <Container>
                    <Row>
                        <Col xs={6}>
                            <img className="about-img" src="./img/about-img.jpg" alt="about img" />
                            
                        </Col>
                        <Col xs={6}>
                            <h4>About Us</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit v Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elit
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit v Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consecpsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elit consectetur adipiscing elit
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit v Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem tur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elit
                            </p>
                            <Button variant="outline-info">Read More</Button>                           
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                        <img className="about-img-2" src="./img/about-2.webp" alt="about img" />
                            
                        </Col>
                    </Row>
                </Container>
            </div>
    )
}

export default About;