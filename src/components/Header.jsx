import React, { Component } from "react";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";


class Header extends Component {

    render(){
        return(
            <Router>
                <div>
                <Navbar bg="light" expand="lg">
                    <Container>
                        <Navbar.Brand href="/">  <img className="logo" src="./img/skill-logo.png" alt="Skill Logo" /> </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link>
                                <Link to="/">Home</Link>
                            </Nav.Link>
                            <Nav.Link >
                                <Link to="/about">About</Link>
                            </Nav.Link>
                            <Nav.Link >
                                <Link to="/contact">Contact Us</Link>
                                
                            </Nav.Link>
                            
                        </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/about" element={<About />} />
                    <Route path="/contact" element={<Contact />} />
                </Routes>
                </div>
            </Router>
        )
    }

}

export default Header;